# Functions runtime as Docker Images

This repository implements a simple concept of
runtime as a docker image for serverless functions.

The idea is that you pass the function code via environment
variable when (currently) running serverless function.

```
export FUNCTION=$(cat my-function.go)
docker run --rm -e FUNCTION registry.gitlab.com/ayufan/serverless-functions/functions/go:1.11.0
```

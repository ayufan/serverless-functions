#!/bin/sh

set -xe

cat <<EOF > function.go
package main

$FUNCTION
EOF

go get -d -v ./...
go install -v ./...

exec app
